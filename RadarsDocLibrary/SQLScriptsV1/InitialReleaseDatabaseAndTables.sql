USE [master]
GO

Create Database RadarsContractTracker;
Go

CREATE TABLE Contract
(
	ContractID int NOT NULL IDENTITY(1,1),
	DateTimeAdded varchar(25) NOT NULL,
	ContractTypeID int NOT NULL,
	AcquisitionCategoryID int NOT NULL,
	ProgramID int NOT NULL,
	CurrentProgramPhaseID int NOT NULL,
	ContractorNameID int NOT NULL,
	ContractDescription text,
	ContractNotes text,
	ContractValue float(15,2),
	RoleID varchar(128),
	IsDeleted bit,
 CONSTRAINT pidx_Contract_ContractID PRIMARY KEY CLUSTERED
 (
    ContractID ASC
 ) 
)
Go

CREATE TABLE ContractAction
(
	ContractActionID int NOT NULL IDENTITY(1,1),
	ContractID int NOT NULL,
	DateTimeAdded varchar(25) NOT NULL,
	ActionTypeID int NOT NULL,
	DOTONumber varchar(25),
	ModNumber varchar(25),
	ActionDescription text,
	AwardDate date,
	EndDate date,
	ContractActionNotes text,
	ContractActionValue float(15,2),
	RoleID varchar(128),
	IsDeleted bit,
 CONSTRAINT pidx_ContractAction_ContractActionID PRIMARY KEY CLUSTERED
 (
	ContractActionID ASC
 )
)
Go

CREATE CLUSTERED INDEX nidx_ContractAction_ContractID   
    ON KeyDate (ContractID);   
GO  

CREATE TABLE CDRL
(
	CDRLID int NOT NULL IDENTITY(1,1),
	ContractActionID int NOT NULL,
	DateTimeAdded varchar(25) NOT NULL,
	CDRLContractLineItemID int NOT NULL,
	DataItemNo varchar(25) NOT NULL,
	Title varchar(50) NOT NULL,
	SubTitle varchar(50),
	Authority varchar(50),
	VendorContractNumber varchar(50),
	ContractRefNo varchar(50),
	CDRLRequiringOfficeID int,
	CDRLDD250ID int,
	CDRLDistStatementID int,
	CDRLFrequencyID int,
	AsOfDate date,
	CDRLStatusID int,
	DetailsAndRemarks text,
	RoleID varchar(128),
	IsDeleted bit,
 CONSTRAINT pidx_CDRL_CDRLID PRIMARY KEY CLUSTERED
 (
	CDRLID ASC
 )
)
Go

CREATE CLUSTERED INDEX nidx_CDRL_ContractActionID   
    ON KeyDate (ContractActionID);   
GO  

CREATE CLUSTERED INDEX nidx_CDRL_DateTimeAdded   
    ON KeyDate (DateTimeAdded);   
GO  

CREATE TABLE CLIN
(
	CLINID int NOT NULL IDENTITY(1,1),
	ContractActionID int NOT NULL,
	DateTimeAdded varchar(25) NOT NULL,
	CLINNumberID int NOT NULL,
	CLINDescription text,
	CLINComments text,
	StartDate date,
	EndDate date,
	CLINValue float(15,2),
	IsDeleted bit,
 CONSTRAINT pidx_CLIN_CLINID PRIMARY KEY CLUSTERED
 (
    CLINID ASC
 ) 
)
Go

CREATE CLUSTERED INDEX nidx_CLIN_ContractActionID   
    ON KeyDate (ContractActionID);   
GO  

CREATE CLUSTERED INDEX nidx_CLIN_DateTimeAdded   
    ON KeyDate (DateTimeAdded);   
GO  

CREATE TABLE KeyDate
(
	KeyDateID int NOT NULL IDENTITY(1,1),
	OwningEntityID int NOT NULL,
	OwningEntityTable varchar(50) NOT NULL,
	KeyDate date NOT NULL,
	KeyDateTypeID int NOT NULL,
	PersonnelID int,
	RelationshipTypeID int,
	EmailReminder bit,
	OnScreenReminder bit, 
 CONSTRAINT pidx_KeyDate_KeyDateID PRIMARY KEY CLUSTERED
 (
    KeyDateID ASC
 ) 
)
Go

CREATE CLUSTERED INDEX nidx_KeyDate_OwningEntityTable_OwningEntityID   
    ON KeyDate (OwningEntityTable,OwningEntityID);   
GO  

CREATE CLUSTERED INDEX nidx_KeyDate_KeyDate   
    ON KeyDate (KeyDate);   
GO  

CREATE CLUSTERED INDEX nidx_KeyDate_KeyDateTypeID   
    ON KeyDate (KeyDatetypeID);   
GO  

CREATE CLUSTERED INDEX nidx_KeyDate_PersonnelID   
    ON KeyDate (PersonnelID);   
GO  

CREATE CLUSTERED INDEX nidx_KeyDate_RelationshipTypeID   
    ON KeyDate (RelationshipTypeID);   
GO  

CREATE TABLE PeriodOfPerformance
(
	PeriodOfPerformnaceID int NOT NULL IDENTITY(1,1),
	PeriodOfPerformanceTypeID int NOT NULL,
	ContractActionID int NOT NULL,
	PeriodOfPerformanceStartDate date NOT NULL,
	PeriodOfPerformanceEndDate date NOT NULL,
 CONSTRAINT pidx_PeriodOfPerformance_PeriodOfPerformanceID PRIMARY KEY CLUSTERED
 (
    PeriodOfPerformanceID ASC
 ) 
)
Go

CREATE CLUSTERED INDEX nidx_PeriodOfPerformance_PeriodOfPerformanceTypeID   
    ON PeriodOfPerformance (PeriodOfPerformanceTypeID);   
GO  

CREATE CLUSTERED INDEX nidx_PeriodOfPerformance_ContractActionID   
    ON PeriodOfPerformance (ContractActionID);   
GO  

CREATE TABLE ContractActionPersonnel
(
	ContractActionPersonnelID int NOT NULL IDENTITY(1,1),
	ContractActionID int NOT NULL,
	RelationshipTypeID int NOT NULL,
	PersonnelID int NOT NULL,
 CONSTRAINT pidx_ContractActionPersonnel PRIMARY KEY CLUSTERED
 (
    ContractActionPersonnelID ASC
 ) 
)
Go

CREATE CLUSTERED INDEX nidx_ContractActionPersonnel_ContractActionID   
    ON ContractActionPersonnel (ContractActionID);   
GO  

CREATE CLUSTERED INDEX nidx_ContractActionPersonnel_RelationshipTypeID   
    ON ContractActionPersonnel (RelationshipTypeID);   
GO  

CREATE CLUSTERED INDEX nidx_ContractActionPersonnel_PersonnelID   
    ON ContractActionPersonnel (PersonnelID);   
GO  

CREATE TABLE AuditTable
(
    AuditTableID int NOT NULL IDENTITY (1,1),
	ChangedTable varchar(50) NOT NULL,
	SchemaVersionID varchar(100) NOT NULL,
	ChangedDateTime varchar(25) NOT NULL,
	ChangedBy varchar(100) NOT NULL,
	SerializedChange text NOT NULL,
 CONSTRAINT pidx_AuditTable PRIMARY KEY CLUSTERED
 (
	AuditTableID ASC
 )
)
Go

CREATE CLUSTERED INDEX nidx_AuditTabole_SchemaVersionID   
    ON AuditTable (SchemaVersionID);   
GO  

CREATE TABLE AuditTableSchemas
(
    SchemaVersionID varchar(100) NOT NULL,
	SchemaXML text NOT NULL,
 CONSTRAINT pidx_AuditTableSchemas PRIMARY KEY CLUSTERED
 (
	SchemaVersionID ASC
 )
)
Go

CREATE TABLE DocumentUploads
(
    DocumentID int NOT NULL IDENTITY (1,1),
	DateTimeAdded varchar(25) NOT NULL,
	OwningEntityTable varchar(50) NOT NULL,
	OwningEntityID int NOT NULL,
	DocumentName varchar(100) NOT NULL,
	DocumentPath varchar(500) NOT NULL,
	Notes text,
 CONSTRAINT pidx_DocumentUploads_DocumentID PRIMARY KEY CLUSTERED
 (
	DocumentID ASC
 )
)
Go

CREATE CLUSTERED INDEX nidx_DocumentUploads_OwningEntityTable_OwningEntityID   
    ON DocumentUploads (OwningEntityTable,OwningEntityID);   
GO  

/****** Lookup Tables ... ******/

CREATE TABLE PeriodOfPerformanceType
(
	PeriodOfPerformanceTypeID int NOT NULL IDENTITY(1,1),
	PeriodOfPerformanceTypeDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_PeriodOfPerformanceType PRIMARY KEY CLUSTERED
 (
    PeriodOfPerformanceTypeID ASC
 ) 
)
Go

CREATE TABLE ContractType
(
	ContractTypeID int NOT NULL IDENTITY(1,1),
	ContractTypeDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_ContractType PRIMARY KEY CLUSTERED
 (
    ContractTypeID ASC
 ) 
)
Go

CREATE TABLE AcquisitionCategory
(
	AcquisitionCategoryID int NOT NULL IDENTITY(1,1),
	AcquisitionCategoryDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_AcquisitionCategory PRIMARY KEY CLUSTERED
 (
    AcquisitionCategoryID ASC
 ) 
)
Go


CREATE TABLE CurrentProgramPhase
(
	CurrentProgramPhaseID int NOT NULL IDENTITY(1,1),
	CurrentProgramPhaseDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_CurrentProgramPhase PRIMARY KEY CLUSTERED
 (
    CurrentProgamPhaseID ASC
 ) 
)
Go

CREATE TABLE CDRLContractLineItem
(
	CDRLContractLineItemID int NOT NULL IDENTITY(1,1),
	CDRLContractLineItemDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_CDRLContractLineItemDesc PRIMARY KEY CLUSTERED
 (
    ContractLineItemID ASC
 ) 
)
Go

CREATE TABLE ContractorName
(
	ContractorNameID int NOT NULL IDENTITY(1,1),
	ContractorNameDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_ContractorName PRIMARY KEY CLUSTERED
 (
    ContractorNameID ASC
 ) 
)
Go

CREATE TABLE ActionType
(
	ActionTypeID int NOT NULL IDENTITY(1,1),
	ActionTypeDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_ActionType PRIMARY KEY CLUSTERED
 (
    ActionTypeID ASC
 ) 
)
Go

CREATE TABLE CDRLRequiringOffice
(
	CDRLRequiringOfficeID int NOT NULL IDENTITY(1,1),
	CDRLRequiringOfficeDesc varchar(100) NOT NULL,
 CONSTRAINT pidx_CDRLRequiringOffice PRIMARY KEY CLUSTERED
 (
    CDRLRequiringOfficeID ASC
 ) 
)
Go

