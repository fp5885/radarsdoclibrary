﻿using Quartz;
using System;
using System.Net;
using System.Net.Mail;
using RadarsDocLibrary;
using RadarsDocLibrary.Models;
using System.Collections.Generic;
using System.Text;
using System.Configuration;


namespace RadarsDocLibrary
{
    public class EmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Inside Scheduler -> EmailJob : Top");
            Utilities lutilRadars = new Utilities();
            List<vwCDRLAlert> loAlerts = lutilRadars.GetEmailAlerts();
            int liAlertCount = loAlerts.Count;
            string ltEmailFrom = ConfigurationManager.AppSettings["EmailFromAddr"];                   
            string ltEmailServer = ConfigurationManager.AppSettings["EmailServer"];
            string ltEmailHour = ConfigurationManager.AppSettings["EmailHour"];
            string ltEmailMinute = ConfigurationManager.AppSettings["EmailMinute"];
            string ltMessageSubject = ConfigurationManager.AppSettings["MessageSubject"];
            string ltMessageBody = ConfigurationManager.AppSettings["MessageBody"];
            string ltMessageType = ConfigurationManager.AppSettings["MessageType"];
            string ltEmailHost = ConfigurationManager.AppSettings["EmailHost"];
            string ltEmailPort = ConfigurationManager.AppSettings["EmailPort"];
            string ltEmailSSLType = ConfigurationManager.AppSettings["EmailSSLType"];
            string ltEmailFormat = ConfigurationManager.AppSettings["EmailFormat"];
            string ltEmailSSL = ConfigurationManager.AppSettings["EmailSSL"];
            string ltEmailTLSPort = ConfigurationManager.AppSettings["EmailTLSPort"];
            string ltEmailSSLPort = ConfigurationManager.AppSettings["EmailSSLPort"];
            string ltEmailTimeout = ConfigurationManager.AppSettings["EmailTimeout"];
            string ltEmailID  = ConfigurationManager.AppSettings["EmailID"];
            string ltEmailPassword  = ConfigurationManager.AppSettings["EmailPassword"];
            string ltEmailErrorAddr  = ConfigurationManager.AppSettings["EmailErrorAddr"];
            string ltRuntimeEnv = ConfigurationManager.AppSettings["RuntimeEnvString"];

            Logger.Info("Inside Scheduler -> EmailJob ");
            
            foreach (var loAlert in loAlerts) 
            {
                personnel loPersonnelForAlert = lutilRadars.GetPersonnelFromAlert(loAlert);
//                string ltEmailForAlert = "error@visioncompsystems.com";
                string ltEmailForAlert = ltEmailErrorAddr;
                if (loPersonnelForAlert != null)
                    ltEmailForAlert = loPersonnelForAlert.Email;
 
//                using (var message = new MailMessage("frank.punzo@visioncompsystems.com", ltEmailForAlert))
                using (var message = new MailMessage(ltEmailFrom, ltEmailForAlert))
                {
                    try
                    {
//                        message.Subject = "CDRL Alert";
                        message.Subject = ltMessageSubject;
                        message.IsBodyHtml = true;
                        StringBuilder lsbMessageBody = new StringBuilder();

                        lsbMessageBody.Append("<a href=\""+ltRuntimeEnv+"/Application/CDRLEntry?whichaction=update&key=" + loAlert.CDRLID + "\">");    
                        lsbMessageBody.Append("*** Click Here - CDRL Alert - Click Here *** </a><br><br>");
                        lsbMessageBody.Append("You are " + loAlert.RelationshipTypeDesc + " and have a " + loAlert.KeyDateTypeDesc + " due : " + loAlert.KeyDate + " for Contractor Name : " + loAlert.ContractorNameDesc + ",  Contract # : " + loAlert.ContractNumber);
                        lsbMessageBody.Append("Please click on the alert above to go to the CDRL, then click [Edit] next the date you are responsible for and complete the task.");
 
                        Logger.Info("Sending Email to : " + ltEmailForAlert + ". Email Msg : " + lsbMessageBody.ToString());
                        message.Body = lsbMessageBody.ToString();

                        bool llEmailSSL = false;

                        if(ltEmailSSL == "true") {
                            llEmailSSL = true;

                            using (SmtpClient client = new SmtpClient
                            {
                                DeliveryMethod=SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = false,
                                Timeout = 30 * 1000,
                                EnableSsl = llEmailSSL,
                                Host = ltEmailHost,
                                Port = Int32.Parse(ltEmailSSLPort),
                                Credentials = new NetworkCredential(ltEmailID, ltEmailPassword)
                            })

                            {
                                Logger.Info("Before send message ");
                                client.Send(message);
                                Logger.Info("After send message ");
                            }
                        }
                        else {
                            llEmailSSL = false;

                            using (SmtpClient client = new SmtpClient
                            {
                                DeliveryMethod=SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = false,
                                Timeout = 30 * 1000,
                                EnableSsl = llEmailSSL,
                                Host = ltEmailHost,
                                Port = Int32.Parse(ltEmailPort),
//                                Credentials = new NetworkCredential(ltEmailID, ltEmailPassword)
                            })

                            {
                                Logger.Info("Before send message ");
                                client.Send(message);
                                Logger.Info("After send message ");
                            }

                        }
                        
                    }
                    catch(Exception e)
                    {
                        Logger.Error("Error sending message ");
                        Logger.Error("Error Message : " + e.Message);
                        Logger.Error("Error stack trace : " + e.StackTrace);
                    }
                }
            }
        }
    }

}