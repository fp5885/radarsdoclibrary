﻿/* 
* Need to parameterize everything in the jobscheduler and email job and add parameters to web.config
 * * * * to do...
 * add cancel to slin???
 * add cancel to CDRL Date??
 * 
 * add "Back to ?" to documents screen (and all associated coding)
 * 
 * * 
 *        ***BUG*** Timezone issues. 
 *      ***BUG*** The user database for cdrl is pointing to the one for radarsfleettracker. Need to configure something in IIS
 * *         ***TODO*** Add "Back" buttons to documents
 *
 * *    There is a timezone issue. When adding OptionPeriods or ContractActionPersonnel early 
 *        morning, the dates chosen are showing up in the jqgrid as the day before. 
 * *                          
 *                 

 * *  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Reflection;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using RadarsDocLibrary.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using RadarsDocLibrary;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Security.Principal;
using System.Threading;
using System.Collections;

namespace RadarsDocLibrary.Controllers
{
    [Authorize]
//    public class ApplicationController : Controller
    public class ApplicationController : AccountController
    {
        Utilities goUtilities = new Utilities();

        public ActionResult AppError()
        {
            return View();
        }

        public ActionResult AuthError()
        {
            return View();
        }

        public ActionResult DocLibEntry()
        {
            return View();
        }

        public ActionResult DocumentUploadEntry()
        {
// this function gets called via ajax from the ContractActionEntry form
            string ltAction = "nothing";
            string ltContractActionPersonnelID = "0";

            if(Request.QueryString["whichaction"] != null)
                ltAction = Request.QueryString["whichaction"];
            
            switch (ltAction.ToLower())
            {
                case "newrecord":
                        DocumentUploads2 loDoc = new DocumentUploads2();
                        loDoc.DateTimeAdded = DateTime.Now.ToString();
                        loDoc.OwningEntityID = 0;
                        loDoc.OwningEntityTable = "DocumentCat";
//                    ContractActionPersonnel loCAP = new ContractActionPersonnel();
//                    System.Web.HttpContext.Current.Session["ContractActionPersonnelID"] = null;
//                    loCAP.ContractActionID = Int32.Parse(System.Web.HttpContext.Current.Session["CurrentContractActionID"].ToString());
                    return PartialView(loDoc);
                    break;
//                case "update":
//                        DocumentUploads2 loDoc = new DocumentUploads2();
//                        loDoc.DateTimeAdded = DateTime.Now.ToString();
//                        loDoc.OwningEntityID = 0;
//                        loDoc.OwningEntityTable = "DocumentCat";
//                    return PartialView(loDoc);
//                    break;
            }

            return PartialView();
        }

        [HttpPost]
//        [ValidateAntiForgeryToken]
        public JsonResult DocumentUploadAjax(String DocumentID, String DateTimeAdded, String OwningEntityTable, String OwningEntityID, String DocumentName)
        {
            var file = Request.Files[0];
            int liDocID = 0;
//            var fileName = Path.GetFileName( file.FileName );
//            var path = Path.Combine( Server.MapPath( "~/Junk/" ) , fileName );
//            file.SaveAs( path );    


            try
            {
                if (file != null && file.ContentLength > 0)
                {
//   Future enhancement ... should put this in a transaction that we can roll back if one part fails. 
                    RadarsFleetManagerEntities radarsDB = new RadarsFleetManagerEntities();
                    DocumentUploads2 loDocUpload = new DocumentUploads2();
                    loDocUpload.DocumentID = Int32.Parse(DocumentID);
                    loDocUpload.DocumentName = DocumentName;
                    loDocUpload.OwningEntityID = Int32.Parse(OwningEntityID);
                    loDocUpload.OwningEntityTable = OwningEntityTable;
                    loDocUpload.DateTimeAdded = DateTimeAdded;
                    loDocUpload.DocumentPath = "temp";
                    radarsDB.DocumentUploads2.Add(loDocUpload);
                    radarsDB.SaveChanges();
                    if(!goUtilities.AddAuditRecord(User, loDocUpload, "Add"))
                    {
                            Logger.Error("Error Adding " + loDocUpload.GetType().Name + " Audit record for : " + User.Identity.Name);
                            throw new InvalidDataException();
                    }

                    string ltFileName = Path.GetFileName(file.FileName);
                    string ltRadarsUploadLoc = goUtilities.getSettingsVal("uploadedfilelocation");

                    ltFileName = "DocID"+"_"+ loDocUpload.DocumentID + "_" + loDocUpload.DocumentName + "_" + ltFileName;
                    liDocID = loDocUpload.DocumentID;                    
            
                    if(System.Web.HttpContext.Current.Session["CurrentDocUploadList"] != null)
                        if(System.Web.HttpContext.Current.Session["CurrentDocUploadList"].ToString().Contains(','))
                            System.Web.HttpContext.Current.Session["CurrentDocUploadList"] += ","+ ltFileName;
                        else
                            System.Web.HttpContext.Current.Session["CurrentDocUploadList"] += ltFileName;
                    else
                        System.Web.HttpContext.Current.Session["CurrentDocUploadList"] += ltFileName;

                                

//                                string ltOutputPath = Path.Combine("c:\\Files\\RadarsUploads\\", ltFileName);
                    string ltOutputPath = Path.Combine(@ltRadarsUploadLoc, ltFileName);
                    // Prepend path with document id to avoid duplicates
                    loDocUpload.DocumentPath = ltOutputPath;
                    radarsDB.DocumentUploads2.Attach(loDocUpload);
                    radarsDB.Entry(loDocUpload).State = System.Data.Entity.EntityState.Modified;
                    radarsDB.SaveChanges();
                    if(!goUtilities.AddAuditRecord(User, loDocUpload, "Update"))
                    {
                            Logger.Error("Error Adding " + loDocUpload.GetType().Name + " Audit record for : " + User.Identity.Name);
                            throw new InvalidDataException();
                    }


                    file.SaveAs(ltOutputPath);                    
                    if(!goUtilities.AddAuditRecord(User, loDocUpload, "DocUpload"))
                    {
                            Logger.Error("Error Adding " + loDocUpload.GetType().Name + " Audit record for : " + User.Identity.Name);
                            throw new InvalidDataException();
                    }
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Successfully uploaded " + DocumentName + " into database.";
                }
                else
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Error with document upload. Content length = 0";
                    Logger.Error("Error with document upload. Document is null or content length is 0");
                }
            }

            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error("File Error : " + ex);
                throw;
            }

            var jsonData = new
            {
                success = true,
                message = "Successfully uploaded document : " + DocumentName,
                docName = DocumentName,
                docID = liDocID
            };


        return Json(jsonData);
        }





        [HttpPost]
//        [ValidateAntiForgeryToken]
        public JsonResult SaveJSTree(String JsonString)
        {
// browser
//"[{"id":"ajson1","text":"Root","icon":true,"li_attr":{"id":"ajson1"},"a_attr":{"href":"#","id":"ajson1_anchor"},"state":{"loaded":true,"opened":true,"selected":false,"disabled":false},"data":{},"parent":"#","type":"folder"},{"id":"ajson2","text":"Root node 2","icon":true,"li_attr":{"id":"ajson2"},"a_attr":{"href":"#","id":"ajson2_anchor"},"state":{"loaded":true,"opened":true,"selected":false,"disabled":false},"data":{},"parent":"ajson1","type":"folder"},{"id":"ajson3","text":"Child 1","icon":"jstree-file","li_attr":{"id":"ajson3"},"a_attr":{"href":"#","id":"ajson3_anchor"},"state":{"loaded":true,"opened":false,"selected":false,"disabled":false},"data":{},"parent":"ajson2","type":"file"},{"id":"ajson4","text":"Child 2","icon":true,"li_attr":{"id":"ajson4"},"a_attr":{"href":"#","id":"ajson4_anchor"},"state":{"loaded":true,"opened":true,"selected":false,"disabled":false},"data":{},"parent":"ajson2","type":"folder"},{"id":"ajson5","text":"Child 3","icon":true,"li_attr":{"id":"ajson5"},"a_attr":{"href":"#","id":"ajson5_anchor"},"state":{"loaded":true,"opened":false,"selected":false,"disabled":false},"data":{},"parent":"ajson4","type":"folder"},{"id":"ajson6","text":"File Child2","icon":"jstree-file","li_attr":{"id":"ajson6"},"a_attr":{"href":"#","id":"ajson6_anchor"},"state":{"loaded":true,"opened":false,"selected":false,"disabled":false},"data":{},"parent":"ajson4","type":"file"},{"id":"218","text":"hello","icon":false,"li_attr":{"id":"j1_1"},"a_attr":{"href":"#","id":"j1_1_anchor"},"state":{"loaded":true,"opened":false,"selected":false,"disabled":false},"data":{"docid":218},"parent":"ajson2","type":"default"},{"id":"219","text":"hello2","icon":false,"li_attr":{"id":"j1_2"},"a_attr":{"href":"#","id":"j1_2_anchor"},"state":{"loaded":true,"opened":false,"selected":false,"disabled":false},"data":{"docid":219},"parent":"#","type":"default"}]"
// here
//	JsonString	"[{\"id\":\"ajson1\",\"text\":\"Root\",\"icon\":true,\"li_attr\":{\"id\":\"ajson1\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson1_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"#\",\"type\":\"folder\"},{\"id\":\"ajson2\",\"text\":\"Root node 2\",\"icon\":true,\"li_attr\":{\"id\":\"ajson2\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson2_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson1\",\"type\":\"folder\"},{\"id\":\"ajson3\",\"text\":\"Child 1\",\"icon\":\"jstree-file\",\"li_attr\":{\"id\":\"ajson3\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson3_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson2\",\"type\":\"file\"},{\"id\":\"ajson4\",\"text\":\"Child 2\",\"icon\":true,\"li_attr\":{\"id\":\"ajson4\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson4_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson2\",\"type\":\"folder\"},{\"id\":\"ajson5\",\"text\":\"Child 3\",\"icon\":true,\"li_attr\":{\"id\":\"ajson5\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson5_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson4\",\"type\":\"folder\"},{\"id\":\"ajson6\",\"text\":\"File Child2\",\"icon\":\"jstree-file\",\"li_attr\":{\"id\":\"ajson6\"},\"a_attr\":{\"href\":\"#\",\"id\":\"ajson6_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson4\",\"type\":\"file\"},{\"id\":\"1004\",\"text\":\"ttt\",\"icon\":false,\"li_attr\":{\"id\":\"j1_1\"},\"a_attr\":{\"href\":\"#\",\"id\":\"j1_1_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{\"docid\":1004},\"parent\":\"ajson1\",\"type\":\"default\"}]"	string


// need to figure out how to get the json data passed into a variable... this isn't working between doclibentry (jsontree) 
// and here. Used cdrl contractactionentry and OptionPeriodEntry as template
// here we will process the jstree

//{
//\"id\":\"ajson1\"
//,\"text\":\"Root\"
//,\"icon\":true
//,\"li_attr\":{\"id\":\"ajson1\"}
//,\"a_attr\":{\"href\":\"x1\",\"id\":\"ajson1_anchor\"}
//,\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false}
//,\"data\":{}
//,\"parent\":\"x2\"
//,\"type\":\"folder\"
//}

//	var jsstr =	"[{\"id\":\"ajson1\",\"text\":\"Root\",\"icon\":true,\"li_attr\":{\"id\":\"ajson1\"},\"a_attr\":{\"href\":\"x1\",\"id\":\"ajson1_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"x2\",\"type\":\"folder\"},{\"id\":\"ajson2\",\"text\":\"Root node 2\",\"icon\":true,\"li_attr\":{\"id\":\"ajson2\"},\"a_attr\":{\"href\":\"x3\",\"id\":\"ajson2_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson1\",\"type\":\"folder\"},{\"id\":\"ajson3\",\"text\":\"Child 1\",\"icon\":\"jstree-file\",\"li_attr\":{\"id\":\"ajson3\"},\"a_attr\":{\"href\":\"x4\",\"id\":\"ajson3_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson2\",\"type\":\"file\"},{\"id\":\"ajson4\",\"text\":\"Child 2\",\"icon\":true,\"li_attr\":{\"id\":\"ajson4\"},\"a_attr\":{\"href\":\"x5\",\"id\":\"ajson4_anchor\"},\"state\":{\"loaded\":true,\"opened\":true,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson2\",\"type\":\"folder\"},{\"id\":\"ajson5\",\"text\":\"Child 3\",\"icon\":true,\"li_attr\":{\"id\":\"ajson5\"},\"a_attr\":{\"href\":\"x6\",\"id\":\"ajson5_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson4\",\"type\":\"folder\"},{\"id\":\"ajson6\",\"text\":\"File Child2\",\"icon\":\"jstree-file\",\"li_attr\":{\"id\":\"ajson6\"},\"a_attr\":{\"href\":\"x7\",\"id\":\"ajson6_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{},\"parent\":\"ajson4\",\"type\":\"file\"},{\"id\":\"1004\",\"text\":\"ttt\",\"icon\":false,\"li_attr\":{\"id\":\"j1_1\"},\"a_attr\":{\"href\":\"x8\",\"id\":\"j1_1_anchor\"},\"state\":{\"loaded\":true,\"opened\":false,\"selected\":false,\"disabled\":false},\"data\":{\"docid\":1004},\"parent\":\"ajson1\",\"type\":\"default\"}]";
            List<JsTreePost> loJSNodes = null;
            try
            {
                loJSNodes = new JavaScriptSerializer().Deserialize<List<JsTreePost>>(JsonString);
                foreach(JsTreePost loJSNode in loJSNodes)
                {
                    Logger.Info("DBID Should be : " + loJSNode.data.docid + " DBParent Should be : " + loJSNode.data.parentid);
                    Logger.Info("ID:" + loJSNode.id + " Text:" + loJSNode.text + " Icon:" + loJSNode.icon + " liattr:" + loJSNode.li_attr.ToString() + " aattr:" + loJSNode.a_attr.ToString() + " State:" + loJSNode.state.ToString() + " Data:" + loJSNode.data.ToString() + " Parent:" + loJSNode.parent + " Type:" + loJSNode.parent );
                }

                Logger.Info("Inside SaveJSTree function");
            }

            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error("File Error : " + ex);
                throw;
            }

            var jsonData = new
            {
                success = true,
                message = "successful",
            };


        return Json(jsonData);
        }

        private void InterceptError(string message)
        {
            var cc = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = cc;
            Logger.Error(message);
//            Debug.WriteLine(message);
        }

        private object DeserializeObject(Dictionary<string, object> dictItem, Type tip)
        {
             
            var newInstance = Activator.CreateInstance(tip);
            var isDictionary = (tip.GetInterface("IDictionary") != null);
            PropertyInfo p = null;
            foreach (var k in dictItem.Keys)
            {
                var ser=  new JavaScriptSerializer().Serialize(dictItem[k]);
 
                 
                Type tipP = null;
                if (isDictionary)
                {
                    tipP = tip.GetGenericArguments()[1];
                }
                else
                {
                     
                    try
                    {
                        p = tip.GetProperty(k);
                        if (p == null)
                            throw new ArgumentNullException(k);
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine(ex.Message);                        
                        InterceptError("missing property:" + k + " to class" + tip.FullName + " value:" + ser);
                        continue;
                    }
                    tipP = p.PropertyType;
                }
                 
                 
                if (tipP.IsClass && tipP.FullName != typeof(string).FullName)
                {
                     
                    dynamic val = null;
                    try
                    {
                        val = new JavaScriptSerializer().Deserialize(ser, tipP);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("error for class:" + k + ex.Message);
                        IList arr = dictItem[k] as object[];
                        if (arr == null)
                        {
                            arr = dictItem[k] as ArrayList;
                        }
                        if (arr == null)
                        {                            
                            var t1 = dictItem[k] as Dictionary<string, object>;
                            if (t1 == null)
                            {
                                InterceptError("Not a dictionary, not an array - please contact ignatandrei@yahoo.com for " + k);
                            }
 
                            val = DeserializeObject(dictItem[k] as Dictionary<string, object>, tipP);
                             
                        }
                        else
                        {
                            val = Activator.CreateInstance(tipP);
                            var tipGen=tipP.GetGenericArguments()[0];
                            foreach (var obj in arr)
                            {
                                val.Add((dynamic)DeserializeObject(obj as Dictionary<string, object>, tipGen));
                            }
                        }
                         
 
                    }
 
                    if (isDictionary)
                    {
                        ((IDictionary)newInstance).Add(k, Convert.ChangeType(val, tipP));
                    }
                    else
                    {
                        p.SetValue(newInstance, Convert.ChangeType(val, tipP), null);
 
                    }
                }
                else//simple int , string, 
                {
                    try
                    {
                        if (isDictionary)
                        {
                            ((IDictionary)newInstance).Add(k, Convert.ChangeType(dictItem[k], tipP));
                        }
                        else
                        {
                            p.SetValue(newInstance, Convert.ChangeType(dictItem[k], tipP), null);
                        }
                    }
                    catch (Exception ex)
                    {
                        InterceptError("!!!not a simple property " + k + " from " + tip + " value:" + ser);
                    }
                }
            }
            return newInstance;
        }



        public JsonResult DocUploadJson()
        {
// process = 1) user clicks Upload Doc 2) The DocUploadDialog loads on screen 2) User chooses file and clicks save 
//       3) DocUploadEntry controller post function executes and uploads the document to the file system, creates db record and creates 
//       or updates the CurrentDocUploadList session var 4) The DocUploadDialog close event fires and executes a refresh on 
//       the jstree1 object 5) jstree1 is setup to call DocUploadJson() to get it's data. 6) DocUploadJson reads the data from the 
//       CurrentDocUploadList created/updated in step 3 and returns the json to update jstree1
            
            var nodes = new List<JsTreeModel>();
            var DocList = new String[] {};

            //process currentdocuploadlist
            if (System.Web.HttpContext.Current.Session["CurrentDocUploadList"] != null)
            {
                if(System.Web.HttpContext.Current.Session["CurrentDocUploadList"].ToString().Contains(","))
                {
                    DocList = System.Web.HttpContext.Current.Session["CurrentDocUploadList"].ToString().Split(',');
                }
                else
                {
                    DocList[0] = System.Web.HttpContext.Current.Session["CurrentDocUploadList"].ToString();
                }
                foreach(String ltDoc in DocList) {
                    nodes.Add(new JsTreeModel() { id=ltDoc,parent="#",text=ltDoc});
                }
            }
            else
            {
                nodes.Add(new JsTreeModel() { id="1", parent="#",text="Error Uploading Document"});
            }
            return Json(nodes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateSiteContent(string contentid)
        {
            return View(goUtilities.GetContentRecord(contentid));
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSiteContent(sitecontent model, string returnUrl, string whichaction)
        {
            if (goUtilities.DoesUserHaveClaim(User, "Admin"))
            {
                if (ModelState.IsValid)
                {
                    //                Utilities lutilRadarUtils = new Utilities();
                    //first check wether person was searching or updating
                    switch (whichaction.ToLower())
                    {
                        case "save data":
                            RadarsContractTrackerEntities radarsDB = new RadarsContractTrackerEntities();

                            //select record into modelry and call DataUpdate view.
                            radarsDB.sitecontents.Attach(model);
                            radarsDB.Entry(model).State = System.Data.Entity.EntityState.Modified;

                            radarsDB.SaveChanges();
                            if(!goUtilities.AddAuditRecord(User, model, "Update"))
                            {
                                    Logger.Error("Error Adding " + model.GetType().Name + " Audit record for : " + User.Identity.Name);
                                    throw new InvalidDataException();
                            }



                            System.Web.HttpContext.Current.Session["DisplayMessage"] = "Site Content ID : " + model.contentid + " has been successfully updated.";
                            return View("UpdateSiteContent");
                            break;
                        case "cancel":
                            // need to pull the current serial number...
                            System.Web.HttpContext.Current.Session["DisplayMessage"] = "Site Content Update Cancelled. ";
                            return View("UpdateSiteContent");
                            break;
                    }
                }
            }
            else
            {
                return View("AuthError");
            }
            return View("UpdateSiteContent");
        }
        
        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }
            public string SerialNumber { get; set; }

        }
        
    }
}