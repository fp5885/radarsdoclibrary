﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace RadarsDocLibrary.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IDMLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IDMLogin(string username, string password, string returnUrl)
        {
            if(username == "idmanager" && password == "R@dars")
            {
                var claims = new Claim[]{
                  new Claim("name", "fpunzo"),
                  new Claim("role", "Admin"),
                };
                var id = new ClaimsIdentity(claims, "Cookies");
                Request.GetOwinContext().Authentication.SignIn(id);
//                return Redirect(returnUrl);
                return Redirect("~/idm");
            }
            
            return View();
        }


       public ActionResult About()
        {
            // when production... check to see if logged in if not then redirect to login
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            // when production... check to see if logged in if not then redirect to login
            ViewBag.Message = "Your contact page.";

            return View();
        }

	}
}