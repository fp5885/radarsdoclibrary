using IdentityManager;
using IdentityManager.Configuration;
using IdentityManager.AspNetIdentity;
using IdentityManager.Core.Logging;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Owin;
using RadarsDocLibrary.Models;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;

[assembly: OwinStartupAttribute(typeof(RadarsDocLibrary.Startup))]
namespace RadarsDocLibrary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Logger.Info("Application Starting...");
            app.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies",
//                LoginPath = new PathString("/Home/IDMLogin")
            });

            ConfigureAuth(app);

            app.Map("/idm", idm =>
            {
                var factory = new IdentityManagerServiceFactory();
                factory.IdentityManagerService = new Registration<IIdentityManagerService, ApplicationIdentityManagerService>();
                factory.Register(new Registration<ApplicationUserManager>());
                factory.Register(new Registration<ApplicationUserStore>());
                factory.Register(new Registration<ApplicationRoleManager>());
                factory.Register(new Registration<ApplicationRoleStore>());
                factory.Register(new Registration<ApplicationDbContext>());

                idm.UseIdentityManager(new IdentityManagerOptions { 
                    Factory = factory,
                   SecurityConfiguration = new HostSecurityConfiguration
                    {
                        HostAuthenticationType = "Cookies",
//                        NameClaimType = "Name", 
//                        RoleClaimType = "role",
//                        AdminRoleName = "IDManager"
                        NameClaimType = "name", 
                        RoleClaimType = "role",
                        AdminRoleName = "IDManager"
                    }
//                      Factory = factory
                });
            });

        }
    }

    public class ApplicationUserManager : UserManager<ApplicationUser,string>
    {
         public ApplicationUserManager(ApplicationUserStore usrStore) : base(usrStore)
        {
            
        }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(ApplicationDbContext ctx)
            : base(ctx)
        {
           
        }
    }

    public class ApplicationRoleStore : RoleStore<IdentityRole>
    {
        public ApplicationRoleStore(ApplicationDbContext ctx)
            :base(ctx)
        {

        }
    }

    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(ApplicationRoleStore roleStore)
        :base(roleStore)
        {
            
        }        

    }

    public class ApplicationIdentityManagerService : AspNetIdentityManagerService<ApplicationUser, string, IdentityRole, string>
    {
        public ApplicationIdentityManagerService(ApplicationUserManager userMgr, ApplicationRoleManager roleMgr )
            : base(userMgr, roleMgr)
        {

        }

    }
}
