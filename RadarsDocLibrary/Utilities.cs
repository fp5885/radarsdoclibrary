using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RadarsDocLibrary.Models;
using System.Text;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Principal;
using System.Security.Claims;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Xml;
using System.Reflection;

//using System.Data.Entity.Core.Objects;

namespace RadarsDocLibrary
{
    public class Utilities
    {
/*        public List<ApplicationUser> GetUsersInRole(string roleName)
        {
            var dbctxt = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbctxt));
            
            var role = roleManager.FindByName(roleName).Users.First();
            var usersInRole = dbctxt.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(role.RoleId)).ToList();
            return usersInRole;
        }
*/

        public List<PeriodOfPerformance> GetPeriodOfPerformance(string ptContractAction)
        {
            // this function must recieve the query to execute, execute it and then 
            // populate the list of pmradarstracker records and return the list
            var ctxPMDB = new RadarsContractTrackerEntities();
//            var ctxPMDB = new RadarsDocLibrary.Models.RadarsContractTrackerEntities();
            var lpmrReturnList = ctxPMDB.Database.SqlQuery<PeriodOfPerformance>("Select * from PeriodOfPerformance").ToList<PeriodOfPerformance>();
            Logger.Info("InsideExecuteQuery. Number of rows returned = " + lpmrReturnList.Count());
            return lpmrReturnList;
        }

        public bool DoesContractActionExist(int piContractActionID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var liContractAction = ctxPMDB.Database.ExecuteSqlCommand("SELECT COUNT(*) FROM ContractAction WHERE ContractActionID = " + piContractActionID);
            if (liContractAction > 0)
                return true;
            else 
                return false;
        }

        public string GetAppContext(string ptWholeURI)
        {
            string ltJustTheContextPath = ptWholeURI;
            return ltJustTheContextPath;
        }

        public Boolean DoesUserHaveClaim(IPrincipal pipUser, string ptClaimName)
        {
            Logger.Info("Inside DoesUserHaveClaim utility function. Checking User : " + pipUser.Identity.GetUserName() + " for role : " + ptClaimName);
            // Cast the Thread.CurrentPrincipal
            ClaimsPrincipal lcp = pipUser as ClaimsPrincipal;
            return lcp.HasClaim("role", ptClaimName);
        }

        public string GetSiteContent (string ptContentID)
        {
//            string ltQuery = "SELECT contentdisplay From RadarsFleetManager.dbo.sitecontent WHERE contentid = '" + ptContentID + "'";
            string ltQuery = "SELECT contentdisplay From RadarsContractTracker.dbo.sitecontent WHERE contentid = '" + ptContentID + "'";
            SqlConnection sqlcnxnRadars = GetDatabaseConnection(); 

            // Create the Command and Parameter objects.
            SqlCommand command = new SqlCommand(ltQuery, sqlcnxnRadars);
            string ltRetval = null;

            try
            {
                sqlcnxnRadars.Open();
                SqlDataReader sqlrdrContent = command.ExecuteReader();
                while (sqlrdrContent.Read())
                {
                    ltRetval = (string) sqlrdrContent[0];
                }
                sqlrdrContent.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return ltRetval;
        }

        public SqlConnection GetDatabaseConnection()
        {
            SqlConnection dbcnxnRadarsDB = null; 
            try
            {
                dbcnxnRadarsDB = new SqlConnection(GetConnectionString());
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
                dbcnxnRadarsDB.Close();
                dbcnxnRadarsDB = null;
            }
            return dbcnxnRadarsDB;
        }

        public string GetConnectionString()
        {
            return GetConnectionString("DefaultConnection");
        }

        public string GetConnectionString(string ptConnectionName)
        {
            string ltCnxString = null;
            try 
            {
                ltCnxString = System.Configuration.ConfigurationManager.ConnectionStrings[ptConnectionName].ConnectionString; 
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            
            return ltCnxString;
        }

        public string GetCurrentDate()
        {
            return DateTime.Now.ToString();
        }

        public string GetCurrentContractNumber(int piCurrentContractID)
        {
            string ltCurrentContractNumber = "";

            try
            {
                var ctxPMDB = new RadarsContractTrackerEntities();
                var loContract = ctxPMDB.Contracts.SqlQuery("SELECT * FROM Contract WHERE ContractID = " + piCurrentContractID).Single();
                Logger.Info("InsideGetCurrentContractNumber " + "SELECT * FROM Contract WHERE ContractID = " + piCurrentContractID);
                if(loContract.ContractNumber != null)
                    ltCurrentContractNumber = loContract.ContractNumber;
                else
                    ltCurrentContractNumber = "Error Retrieving Contract";
            }
               catch(DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }
                
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

            return ltCurrentContractNumber;
        }

        public Contract GetContract(int piCurrentContractID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loContract = ctxPMDB.Contracts.SqlQuery("SELECT * FROM Contract WHERE ContractID = " + piCurrentContractID).Single();
            Logger.Info("InsideGetContract " + "SELECT * FROM Contract WHERE ContractID = " + piCurrentContractID);
            return loContract;
            
        }

        public void DeleteContractRecord(string ptKey, IPrincipal pipUser )
        {
            Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting Contract ID : " + ptKey );
            int liID = Int32.Parse(ptKey);
            var ctxPMDB = new RadarsContractTrackerEntities();
            var ctxRadarsFleetDB = new RadarsFleetManagerEntities();
            CascadeDeleteContractAction(ctxPMDB, ptKey, pipUser);
            CascadeDeleteDocuments(ctxRadarsFleetDB, "Contract", Int32.Parse(ptKey), pipUser);

            try
            {
                
                Contract lcurrentrecord = GetContract(liID);
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                ctxPMDB.Entry(lcurrentrecord).State = EntityState.Detached;
                lcurrentrecord.IsDeleted = true;

                ctxPMDB.Contracts.Attach(lcurrentrecord);
                ctxPMDB.Entry(lcurrentrecord).State = System.Data.Entity.EntityState.Modified;

                ctxPMDB.SaveChanges();
                Logger.Warn("WARNING : Cascading delete to Contract : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + ptKey );
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void DeleteContractActionRecord(string ptKey, IPrincipal pipUser )
        {
            Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting ContractAction ID : " + ptKey );
            int liID = Int32.Parse(ptKey);
            var ctxPMDB = new RadarsContractTrackerEntities();
            var ctxRadarsFleetDB = new RadarsFleetManagerEntities();
            CascadeDeleteDocuments(ctxRadarsFleetDB, "Contract", Int32.Parse(ptKey), pipUser);

            try
            {
                ContractAction lcurrentrecord = GetContractAction(liID);
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                ctxPMDB.Entry(lcurrentrecord).State = EntityState.Detached;
                lcurrentrecord.IsDeleted = true;

                ctxPMDB.ContractActions.Attach(lcurrentrecord);
                ctxPMDB.Entry(lcurrentrecord).State = System.Data.Entity.EntityState.Modified;

                ctxPMDB.SaveChanges();
                Logger.Warn("WARNING : Cascading delete to Contract : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + ptKey );
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void DeleteCDRLRecord(string ptKey, IPrincipal pipUser )
        {
            Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting CDRL ID : " + ptKey );
            int liID = Int32.Parse(ptKey);
            var ctxPMDB = new RadarsContractTrackerEntities();
            var ctxRadarsFleetDB = new RadarsFleetManagerEntities();
            CascadeDeleteDocuments(ctxRadarsFleetDB, "CDRL", Int32.Parse(ptKey), pipUser);
            CascadeDeleteKeyDates(ctxPMDB, "CDRL", Int32.Parse(ptKey), pipUser);

            try
            {
                CDRL lcurrentrecord = GetSelectedCDRLRecord(liID);
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                ctxPMDB.Entry(lcurrentrecord).State = EntityState.Detached;
//                CDRL rec = GetSelectedCDRLRecord(liID);
                lcurrentrecord.IsDeleted = true;

                ctxPMDB.CDRLs.Attach(lcurrentrecord);
                ctxPMDB.Entry(lcurrentrecord).State = System.Data.Entity.EntityState.Modified;

                ctxPMDB.SaveChanges();
                Logger.Warn("WARNING : Cascading delete to Contract : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + ptKey );
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void DeleteCLINRecord(string ptKey, IPrincipal pipUser )
        {
            Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting CLIN ID : " + ptKey );
            int liID = Int32.Parse(ptKey);
            var ctxPMDB = new RadarsContractTrackerEntities();
            var ctxRadarsFleetDB = new RadarsFleetManagerEntities();
            CascadeDeleteDocuments(ctxRadarsFleetDB, "CLIN", Int32.Parse(ptKey), pipUser);
            CascadeDeleteSLINs(ctxPMDB, Int32.Parse(ptKey), pipUser);

            try
            {
                CLIN lcurrentrecord = GetSelectedCLINRecord(liID);
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                ctxPMDB.Entry(lcurrentrecord).State = EntityState.Detached;
                lcurrentrecord.IsDeleted = true;

                ctxPMDB.CLINs.Attach(lcurrentrecord);
                ctxPMDB.Entry(lcurrentrecord).State = System.Data.Entity.EntityState.Modified;

                ctxPMDB.SaveChanges();
                Logger.Warn("WARNING : Cascading delete to Contract : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + ptKey );
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }
        public void CascadeDeleteContractAction(RadarsContractTrackerEntities pctxPMDB, string ptKey, IPrincipal pipUser )
        {
            int liCAID = Int32.Parse(ptKey);
            var laContractActionRecords = pctxPMDB.ContractActions.SqlQuery("SELECT * FROM [dbo].[ContractAction] WHERE ContractID = "+liCAID+" AND ([IsDeleted] is NULL OR [IsDeleted] = 0)").AsNoTracking();
//            var laContractActionRecords = pctxPMDB.ContractActions.Where(c => c.ContractID.Equals(liCAID));
            var lctxRadarsFleetDB = new RadarsFleetManagerEntities();
            try
            {
                if (laContractActionRecords.Count() > 0)
                {
                    foreach (ContractAction objR in laContractActionRecords.ToList())
                    {
                        int liID = objR.ContractActionID;
                        CascadeDeleteCLIN(pctxPMDB, liID, pipUser);
                        CascadeDeleteCDRL(pctxPMDB, liID, pipUser);
                        CascadeDeleteDocuments(lctxRadarsFleetDB, "ContractAction", liID, pipUser);
                        CascadeDeletePeriodOfPerformance(pctxPMDB, liID, pipUser);
                        CascadeDeletePersonnel(pctxPMDB, liID, pipUser);

                        pctxPMDB.Entry(objR).State = EntityState.Detached;
                        ContractAction rec = GetContractAction(liID);
                        rec.IsDeleted = true;

                        pctxPMDB.ContractActions.Attach(rec);
                        pctxPMDB.Entry(rec).State = System.Data.Entity.EntityState.Modified;

                        pctxPMDB.SaveChanges();
                        Logger.Warn("WARNING : Cascading delete to ContractAction : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + ptKey );
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public void CascadeDeleteCDRL(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {

                var laCDRLRecords = pctxPMDB.CDRLs.SqlQuery("SELECT * FROM [dbo].[CDRL] WHERE ContractActionID = "+piID+" AND ([IsDeleted] is NULL OR [IsDeleted] = 0)").AsNoTracking();
//            var laContractActionRecords = pctxPMDB.CDRLs.Where(c => c.ContractActionID.Equals(piID));
            var lctxRadarsFleetDB = new RadarsFleetManagerEntities();
            if (laCDRLRecords.Count() > 0)
            {
                foreach (CDRL objR in laCDRLRecords.ToList())
                {
                    int liID = objR.CDRLID;
                    CascadeDeleteKeyDates(pctxPMDB, "CDRL", liID, pipUser);
                    CascadeDeleteDocuments(lctxRadarsFleetDB, "CDRL", liID, pipUser);

                    pctxPMDB.Entry(objR).State = EntityState.Detached;
                    CDRL rec = GetSelectedCDRLRecord(liID);
                    rec.IsDeleted = true;

                    pctxPMDB.CDRLs.Attach(rec);
                    pctxPMDB.Entry(rec).State = System.Data.Entity.EntityState.Modified;

                    pctxPMDB.SaveChanges();
                    Logger.Warn("WARNING : Cascading delete to ContractAction : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + piID.ToString() );
                }
            }


        }

        public void CascadeDeletePersonnel(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            var laRecords = pctxPMDB.ContractActionPersonnels.Where(c => c.ContractActionID.Equals(piID)).AsNoTracking();
            if (laRecords.Count() > 0)
            {
                foreach (ContractActionPersonnel objR in laRecords.ToList())
                {
                    int liID = objR.ContractActionPersonnelID;
                    DeleteContractActionPersonnel(pctxPMDB, liID, pipUser);
                }
            }            
        }

        public void CascadeDeletePeriodOfPerformance(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            var laRecords = pctxPMDB.PeriodOfPerformances.Where(c => c.ContractActionID.Equals(piID)).AsNoTracking();
            if (laRecords.Count() > 0)
            {
                foreach (PeriodOfPerformance objR in laRecords.ToList())
                {
                    int liID = objR.PeriodOfPerformanceID;
                    DeletePeriodOfPerformance(pctxPMDB, liID, pipUser);
                }
            }            
        }


        public void CascadeDeleteDocuments(RadarsFleetManagerEntities pctxPMDB, string ptDocumentType, int piID, IPrincipal pipUser )
        {
            var laRecords = pctxPMDB.DocumentUploads2.Where(c => c.OwningEntityTable.Equals(ptDocumentType)).AsNoTracking();
            if (laRecords.Count() > 0)
            {
                foreach (DocumentUploads2 objR in laRecords.ToList())
                {
                    int liID = objR.DocumentID;
                    DeleteDocument(liID, pipUser);
                }
            }            
        }

        public void CascadeDeleteKeyDates(RadarsContractTrackerEntities pctxPMDB, string ptDateType, int piID, IPrincipal pipUser )
        {
            var laRecords = pctxPMDB.KeyDates.Where(c => c.OwningEntityTable.Equals(ptDateType)).AsNoTracking();
            if (laRecords.Count() > 0)
            {
                foreach (KeyDate objR in laRecords.ToList())
                {
                    int liID = objR.KeyDateID;
                    DeleteKeyDate(liID, pipUser);
                }
            }            
        }

        public void CascadeDeleteCLIN(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            var laCLINRecords = pctxPMDB.CLINs.SqlQuery("SELECT * FROM [dbo].[CLIN] WHERE ContractActionID = "+piID+" AND ([IsDeleted] is NULL OR [IsDeleted] = 0)").AsNoTracking();
            if (laCLINRecords.Count() > 0)
            {
                foreach (CLIN objR in laCLINRecords.ToList())
                {
                    int liID = objR.CLINID;
                    DeleteSLIN(pctxPMDB, liID, pipUser);

                    pctxPMDB.Entry(objR).State = EntityState.Detached;
                    CLIN rec = GetSelectedCLINRecord(liID);
                    rec.IsDeleted = true;

                    pctxPMDB.CLINs.Attach(rec);
                    pctxPMDB.Entry(rec).State = System.Data.Entity.EntityState.Modified;

                    pctxPMDB.SaveChanges();
                    Logger.Warn("WARNING : Cascading delete to CLIN : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of Contract : " + piID.ToString() );
                }
            }            
        }

        public void CascadeDeleteSLINs(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            try
            {
                var laSLINRecords = pctxPMDB.SLINs.SqlQuery("SELECT * FROM [dbo].[SLIN] WHERE CLINID = "+piID+" AND ([IsDeleted] is NULL OR [IsDeleted] = 0)").AsNoTracking();
//                var laSLINRecords = pctxPMDB.SLINs.Where((c => c.CLINID.Equals(piID)) && (c.IsDeleted.Equals(0) || c.IsDeleted ?? null));
                if (laSLINRecords.Count() > 0)
                {
                    foreach (SLIN objR in laSLINRecords.ToList())
                    {
                        int liID = objR.SLINID;
                        DeleteSLIN(pctxPMDB, liID, pipUser);
                        Logger.Warn("WARNING : Cascading delete to SLIN : " + liID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of CLIN : " + piID.ToString() );
                    }
                }            
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

            
        }

        public void DeleteSLIN(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            try
            {
                  SLIN lcurrentrecord = GetSelectedSLINRecord(piID);            
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }

                pctxPMDB.Entry(lcurrentrecord).State = EntityState.Detached;
                lcurrentrecord.IsDeleted = true;
                
                pctxPMDB.SLINs.Attach(lcurrentrecord);
                pctxPMDB.Entry(lcurrentrecord).State = System.Data.Entity.EntityState.Modified;

                pctxPMDB.SaveChanges();
                Logger.Warn("WARNING : Cascading delete to SLIN : " + piID.ToString() + " resulting from User : " + pipUser.Identity.GetUserName() + " deletion of CLIN" );
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }
            catch (Exception e)
            {
                Logger.Error("Unknown Exception : ");
                Logger.Error(e);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + e.InnerException.InnerException.Message;
            }
                   
        }

        public void DeletePeriodOfPerformance(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            try
            {
                PeriodOfPerformance lcurrentrecord = new PeriodOfPerformance { PeriodOfPerformanceID = piID };
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                pctxPMDB.PeriodOfPerformances.Attach(lcurrentrecord);
                pctxPMDB.PeriodOfPerformances.Remove(lcurrentrecord);
                pctxPMDB.SaveChanges();
            }

            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }
            catch (Exception e)
            {
                Logger.Error("Unknown Exception : ");
                Logger.Error(e);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + e.InnerException.InnerException.Message;
            }
        }

        public void DeleteContractActionPersonnel(RadarsContractTrackerEntities pctxPMDB, int piID, IPrincipal pipUser )
        {
            try
            {
                ContractActionPersonnel lcurrentrecord = new ContractActionPersonnel { ContractActionPersonnelID = piID };
                if(!AddAuditRecord(pipUser, lcurrentrecord, "Delete"))
                {
                        Logger.Error("Error Adding " + lcurrentrecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }
                pctxPMDB.ContractActionPersonnels.Attach(lcurrentrecord);
                pctxPMDB.ContractActionPersonnels.Remove(lcurrentrecord);
                pctxPMDB.SaveChanges();
            }

            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }
            catch (Exception e)
            {
                Logger.Error("Unknown Exception : ");
                Logger.Error(e);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + e.InnerException.InnerException.Message;
            }
        }


        public Contract GetContractFromContractAction(int piContractActionID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loContractAction = ctxPMDB.ContractActions.SqlQuery("Select * from ContractAction WHERE ContractActionID = " + piContractActionID).Single();
            var loContract = ctxPMDB.Contracts.SqlQuery("SELECT * FROM Contract WHERE ContractID = " + loContractAction.ContractID).Single();
            Logger.Info("InsideGetContractFromContractAction " + "SELECT * FROM Contract WHERE ContractID = " + loContractAction.ContractID);
            return loContract;            
        }

        public PeriodOfPerformance GetPeriodOfPerformance(int piOptionPeriodID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loPeriodOfPerformance = ctxPMDB.PeriodOfPerformances.SqlQuery("SELECT * FROM PeriodOfPerformance WHERE PeriodOfPerformanceID = " + piOptionPeriodID).Single();
            Logger.Info("InsideGetPeriodOfPerformance " + "SELECT * FROM PeriodOfPerformance WHERE PeriodOfPerformanceID = " + piOptionPeriodID);
            return loPeriodOfPerformance;
        }

        public ContractActionPersonnel GetContractActionPersonnel(int piContractActionPersonnelID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loContractActionPersonnel = ctxPMDB.ContractActionPersonnels.SqlQuery("SELECT * FROM ContractActionPersonnel WHERE ContractActionPersonnelID = " + piContractActionPersonnelID).Single();
            Logger.Info("InsideGetPeriodOfPerformance " + "SELECT * FROM ContractActionPersonnel WHERE ContractActionPersonnelID = " + piContractActionPersonnelID);
            return loContractActionPersonnel;
        }

        public KeyDate GetKeyDate(int piKeyDateID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loKeyDate = ctxPMDB.KeyDates.SqlQuery("SELECT [KeyDateID], [OwningEntityID], [OwningEntityTable], [KeyDate] AS KeyDate1, [KeyDateTypeID], [PersonnelID], [RelationshipTypeID], [EmailReminder], [OnScreenReminder], [ObligationMet], [CompletionDate], [Comments] FROM KeyDate WHERE KeyDateID = " + piKeyDateID).Single();
            Logger.Info("InsideGetKeyDate " + "SELECT [KeyDateID], [OwningEntityID], [OwningEntityTable], [KeyDate] AS KeyDate1, [KeyDateTypeID], [PersonnelID], [RelationshipTypeID], [EmailReminder], [OnScreenReminder], [ObligationMet], [CompletionDate], [Comments]  FROM KeyDate WHERE KeyDateID = " + piKeyDateID);
            return loKeyDate;
        }

/*
 * BEGIN Functions to return distinct lookup table names for the search function on the jqGrids
 * 
*/
        public string getAuditChangedBy()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.vwAuditChangedBies.Select(m => m.ChangedBy).ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getAuditChangedTable()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.vwAuditTables.Select(m => m.ChangedTable).ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getAuditUpdateType()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.vwAuditUpdateTypes.Select(m => m.UpdateType).ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getContractTypes()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.ContractTypes.Select(m => m.ContractTypeDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getContractActionTypes()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.ActionTypes.Select(m => m.ActionTypeDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public void SaveContractAction(string ptContractActionJSON, IPrincipal pipUser)
        {

// Current format of the ContractActionJSON string
/*
{ 
 \"__RequestVerificationToken\":\"SdyKxeo3tKUDbnNKilGDTl8n2VHE3WVswJgBwp2Pw-890jPRd8lVETEXK-zlMlsr772pR6O6cCYkD9dUPfYas8o_YuB26ENyaA2Ozo5qCdXpnwcgAOfFz8ACo5moAw9avVE8DkalH4W202sAH7McdA2\",
 \"valueshavechanged\":\"true\",\"ContractID\":\"65\",\"ContractActionID\":\"90\",\"DateTimeAdded\":\"2016-09-11 21:39:25.460\",
 \"ActionTypeID\":\"1\",\"DOTONumber\":\"doto90\",\"ModNumber\":\"mod90\",\"AwardDate\":\"9/11/2016 12:00:00 AM\",
 \"EndDate\":\"9/11/2016 12:00:00 AM\",\"ContractActionValue\":\"90.00\",\"ActionDescription\":\"901\",\"ContractActionNotes\":\"901\"
} 
*/
            int liContractActionID = 0;

            ContractActionJSON ldeserializedContractAction = JsonConvert.DeserializeObject<ContractActionJSON>(ptContractActionJSON);
            if (ldeserializedContractAction.ContractActionID > 0)
               liContractActionID = ldeserializedContractAction.ContractActionID;
            else
               liContractActionID = (int)System.Web.HttpContext.Current.Session["CurrentContractActionID"];

            if (ldeserializedContractAction.valueshavechanged.ToLower() == "true")
            {
                    RadarsContractTrackerEntities radarsDB = new RadarsContractTrackerEntities();
                    ContractAction lContractAction = new ContractAction() { ActionDescription = ldeserializedContractAction.ActionDescription, ActionTypeID = ldeserializedContractAction.ActionTypeID, AwardDate = ldeserializedContractAction.AwardDate, ContractActionID = liContractActionID, ContractActionNotes = ldeserializedContractAction.ContractActionNotes, ContractActionValue = ldeserializedContractAction.ContractActionValue, ContractID = ldeserializedContractAction.ContractID, DateTimeAdded = ldeserializedContractAction.DateTimeAdded, DOTONumber = ldeserializedContractAction.DOTONumber, EndDate = ldeserializedContractAction.EndDate, IsDeleted = ldeserializedContractAction.IsDeleted, ModNumber = ldeserializedContractAction.ModNumber, RoleID = ldeserializedContractAction.RoleID };
                    //save contractaction record
                    radarsDB.ContractActions.Attach(lContractAction);
                    radarsDB.Entry(lContractAction).State = System.Data.Entity.EntityState.Modified;

                    radarsDB.SaveChanges();
                    if(!AddAuditRecord(pipUser, lContractAction, "Update"))
                    {
                            Logger.Error("Error Adding " + lContractAction.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                            throw new InvalidDataException();
                    }

                    System.Web.HttpContext.Current.Session["CurrentContractActionID"] = lContractAction.ContractActionID;
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Successfully updated ContractAction record.";
            }
        }

        public string getSLINNumbers()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.SLINNumbers.Select(m => m.SLINNumberDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCLINNumbers()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CLINNumbers.Select(m => m.CLINNumberDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }
        
        public string getCDRLFrequencies()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CDRLFrequencies.Select(m => m.CDRLFrequencyDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCDRLDistStatements()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CDRLDistStatements.Select(m => m.CDRLDistStatementDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCDRLAppCodes()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CDRLAppCodes.Select(m => m.CDRLAppCodeDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCDRLDD250s()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CDRLDD250.Select(m => m.CDRLDD250Desc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCDRLRequiringOffices()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CDRLRequiringOffices.Select(m => m.CDRLRequiringOfficeDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }


        public string getAcquisitionCategories()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.AcquisitionCategories.Select(m => m.AcquisitionCategoryDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getPrograms()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.Programs.Select(m => m.ProgramDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getCurrentProgramPhases()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.CurrentProgramPhases.Select(m => m.CurrentProgramPhaseDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getContractorNames()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.ContractorNames.Select(m => m.ContractorNameDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);

        }

        public string getKeyDateTypeDescs()
        {
            var ctxPMDB = new RadarsContractTrackerEntities();

            List<string> lstarr = null;

            lstarr = ctxPMDB.KeyDateTypes.Select(m => m.KeyDateTypeDesc).Distinct().ToList();

            return ConvertStringArrayToJQGridString(lstarr);
        }

/*
 * END Functions to return distinct lookup table names for the search function on the jqGrids
 * 
*/

        public List<RoleModel> GetRoles(String ltFilter)
        {
            List <RoleModel> laRoles = new List<RoleModel>() ;

//todo query out the roles (distinct) and return a list of strings containing them. 
            StringBuilder lsbQuery = new StringBuilder();
            lsbQuery.Append("SELECT Name FROM RadarsContractTracker.dbo.AspNetRoles");
            if (ltFilter.Length > 0)
            {
                lsbQuery.Append(" WHERE " + ltFilter);
            }
            SqlConnection sqlcnxnRadars = GetDatabaseConnection(); 
            SqlCommand command = new SqlCommand(lsbQuery.ToString(), sqlcnxnRadars);

            try
            {
                sqlcnxnRadars.Open();
                SqlDataReader sqlrdrRoles = command.ExecuteReader();

                  while (sqlrdrRoles.Read())
                  {
                        laRoles.Add(new RoleModel() { RoleName = sqlrdrRoles.GetString(0)});   
                  }

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return laRoles;  
       }

        public ContractAction GetContractAction(int piCurrentContractActionID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loContractAction = ctxPMDB.ContractActions.SqlQuery("SELECT * FROM ContractAction WHERE ContractActionID = " + piCurrentContractActionID).Single();
            Logger.Info("InsideGetContractAction " + "SELECT * FROM ContractAction WHERE ContractActionID = " + piCurrentContractActionID);
            return loContractAction;

        }
    
        public CDRL GetSelectedCDRLRecord(int piCDRLID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loCDRL = ctxPMDB.CDRLs.SqlQuery("SELECT * FROM CDRL WHERE CDRLID = " + piCDRLID).Single();
            Logger.Info("Inside GetSelecteCDRLRecord " + "SELECT * FROM CDRL WHERE CDRLID = " + piCDRLID);
            return loCDRL;
        }

        public CLIN GetSelectedCLINRecord(int piCLINID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loCLIN = ctxPMDB.CLINs.SqlQuery("SELECT * FROM CLIN WHERE CLINID = " + piCLINID).Single();
            Logger.Info("Inside GetSelecteCLINRecord " + "SELECT * FROM CLIN WHERE CLINID = " + piCLINID);
            return loCLIN;
        }

        public SLIN GetSelectedSLINRecord(int piSLINID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loSLIN = ctxPMDB.SLINs.SqlQuery("SELECT * FROM SLIN WHERE SLINID = " + piSLINID).Single();
            Logger.Info("Inside GetSelecteSLINRecord " + "SELECT * FROM SLIN WHERE SLINID = " + piSLINID);
            return loSLIN;
        }

        public string GetContractForContractActionID(int piContractActionID)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loContractAction = ctxPMDB.ContractActions.SqlQuery("SELECT * FROM ContractAction WHERE ContractActionID = " + piContractActionID).Single();
            var loContract = ctxPMDB.Contracts.SqlQuery("Select * from Contract WHERE ContractID = " + loContractAction.ContractID).Single();
            Logger.Info("InsideGetContractForContractActionID " + "Select * from Contract WHERE ContractID = " + loContractAction.ContractID);
            return loContract.ContractNumber;
        }

        public sitecontent GetContentRecord(string contentid)
        {
            var ctxPMDB = new RadarsDocLibrary.Models.RadarsContractTrackerEntities();
            if (contentid == null)
            {
                var lpmrRetVal = ctxPMDB.sitecontents.SqlQuery("SELECT TOP 1 [contentid],[contentdisplay] FROM [RadarsContractTracker].[dbo].[sitecontent]").Single();
                return lpmrRetVal;
            }
            else
            {
                var lpmrRetVal = ctxPMDB.sitecontents.SqlQuery("SELECT [contentid],[contentdisplay] FROM [RadarsFleetManager].[dbo].[sitecontent] WHERE [contentid] = '" + contentid + "'").Single();
                return lpmrRetVal;
            }
        }

        public string getSettingsVal(string ptSettingName)
        {
             string ltRetVal =  System.Web.Configuration.WebConfigurationManager.AppSettings["uploadedfilelocation"] ;
			if (!String.IsNullOrEmpty(ltRetVal))
			{
				return ltRetVal;
			}
            else
            {
                Logger.Error ("Error: Setting " + ptSettingName + " not found.");
                ltRetVal = "Error setting not found";
            }

            return ltRetVal;
        }


        public string DownloadDocumentFullPath(int piDocumentID)
        {
            // get record
            // return full path
            DocumentUploads2 loDocRecord = GetDocumentRecord(piDocumentID);  

            return loDocRecord.DocumentPath;
        }

        public DocumentUploads2 GetDocumentRecord(int piDocumentID)
        {
            var ctxPMDB = new RadarsFleetManagerEntities();
            return ctxPMDB.DocumentUploads2.SqlQuery("SELECT * FROM [dbo].[DocumentUploads] WHERE [DocumentID] = '" + piDocumentID + "'").Single();
        }

        public byte[] DownloadDocument(int piDocumentID)
        {
            // find record in document table
            // get filepath
            // read file
            // return file bytes
            var ctxPMDB = new RadarsContractTrackerEntities();
            var lpmrDocRecord = GetDocumentRecord(piDocumentID);
            
            return File.ReadAllBytes(lpmrDocRecord.DocumentPath);
        }
 
        public void DeleteDocument(int piDocumentID, IPrincipal pipUser)
        {
            var ctxPMDB = new RadarsFleetManagerEntities();
            try
            {
                Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting Document ID # : " + piDocumentID.ToString() + ".");

                DocumentUploads2 loDocumentRecord = GetDocumentRecord(piDocumentID); 
                string ltFilePath = loDocumentRecord.DocumentPath;

                DocumentUploads2 loDocument = new DocumentUploads2 { DocumentID = piDocumentID, DateTimeAdded = loDocumentRecord.DateTimeAdded, DocumentName = loDocumentRecord.DocumentName, DocumentPath = loDocumentRecord.DocumentPath, OwningEntityID = loDocumentRecord.OwningEntityID, OwningEntityTable = loDocumentRecord.OwningEntityTable, Notes = loDocumentRecord.Notes };

                if(!AddAuditRecord(pipUser, loDocument, "Delete"))
                {
                        Logger.Error("Error Adding " + loDocument.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }


                ctxPMDB.DocumentUploads2.Attach(loDocument);
                ctxPMDB.DocumentUploads2.Remove(loDocument);
                ctxPMDB.SaveChanges();

                // now delete it from the file system.
                File.Delete(ltFilePath);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }

        }

        public void DeleteKeyDate(int piKeyDateID, IPrincipal pipUser)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            try
            {
                Logger.Warn("WARNING : User : " + pipUser.Identity.GetUserName() + " is deleting KeyDate ID # : " + piKeyDateID.ToString() + ".");

                KeyDate loKeyDateRecord = GetKeyDate(piKeyDateID); 

                if(!AddAuditRecord(pipUser, loKeyDateRecord, "Delete"))
                {
                        Logger.Error("Error Adding " + loKeyDateRecord.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }


                ctxPMDB.KeyDates.Attach(loKeyDateRecord);
                ctxPMDB.KeyDates.Remove(loKeyDateRecord);
                ctxPMDB.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Web.HttpContext.Current.Session["DisplayMessage"] = "Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ";
                    Logger.Error("Entity of type : " + eve.Entry.Entity.GetType().Name + " in state : " + eve.Entry.State + " has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Web.HttpContext.Current.Session["DisplayMessage"] += "- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage;
                        Logger.Error("- Property: " + ve.PropertyName + " , Error: " + ve.ErrorMessage);
                    }
                }
                //                    throw;
            }
            catch (DbUpdateException ue)
            {
                Logger.Error("DBUpdate Exception : ");
                Logger.Error(ue);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
            }
            catch (Exception e)
            {
                Logger.Error("Unknown Exception : ");
                Logger.Error(e);
                System.Web.HttpContext.Current.Session["DisplayMessage"] = "Exception : " + e.InnerException.InnerException.Message;
            }

        }

        public bool IsThereAnAlert(IPrincipal pUser)      
        {
            bool llRetVal = false;
            string ltUserName = pUser.Identity.GetUserName();
            DateTime ldCurrentDate = DateTime.Today;

            // check to see if there are any alerts for the current user

            var ctxPMDB = new RadarsContractTrackerEntities();

            int liCount = (from o in ctxPMDB.vwCDRLAlerts
                 where o.UserName == ltUserName 
                 where o.ObligationMet == false
                 where o.KeyDate <= ldCurrentDate
                select o).Count();

            if(liCount > 0) 
                llRetVal = true;

            return llRetVal;
        }

        public string GetAlert(IPrincipal pUser)      
        {
            string ltUserName = pUser.Identity.GetUserName();
            StringBuilder lsbAlertText = new StringBuilder();

            DateTime ldCurrentDate = DateTime.Today;
            string ltCurrentDate = ldCurrentDate.ToString("d");    
            var ctxPMDB = new RadarsContractTrackerEntities();
 
            // check to see if there are any alerts for the current user
            // select from vwCDRLAlert where user = ltUserName and onscreenreminder = 1 and obligationmet = 0 and KeyDate >= currentdate
//           var  loCDRLAlerts = ctxPMDB.Database.SqlQuery<vwCDRLAlert>("SELECT KeyDateID, UserName, EmailReminder, OnScreenReminder, ObligationMet, keydateTypeID, KeyDateTypeDesc, KeyDate, RelationshipTypeID, RelationshipTypeDesc, CDRLID, ContractActionID, CDRLContractLineItemID, CDRLContractLineItemDesc, ContractNumber, ActionTypeID, ActiontypeDesc, ContractTypeID, ContractTypeDesc, ProgramID, ProgramDesc, CurrentProgramPhaseID, CurrentProgramPhaseDesc, ContractorNameID, ContractorNameDesc FROM vwCDRLAlert WHERE UserName = '" + ltUserName + "' AND OnScreenReminder = 1 AND ObligationMet = 0 AND KeyDate <= '" + ltCurrentDate + "'").ToList<vwCDRLAlert>();
           var  loCDRLAlerts = ctxPMDB.Database.SqlQuery<vwCDRLAlert>("SELECT KeyDateID, UserName, EmailReminder, OnScreenReminder, ObligationMet, keydateTypeID, KeyDateTypeDesc, KeyDate, RelationshipTypeID, RelationshipTypeDesc, CDRLID, ContractActionID, CDRLContractLineItemID, CDRLContractLineItemDesc, ContractNumber, ActionTypeID, ActiontypeDesc, ContractTypeID, ContractTypeDesc, ProgramID, ProgramDesc, CurrentProgramPhaseID, CurrentProgramPhaseDesc, ContractorNameID, ContractorNameDesc FROM vwCDRLAlert WHERE UserName = '" + ltUserName + "' AND OnScreenReminder = 1 AND ObligationMet = 0 AND KeyDate <= '" + ltCurrentDate + "'").ToList<vwCDRLAlert>();

            int liAlertCount = loCDRLAlerts.Count;
            int liCtr = 0;

            // get total count of alerts
            // create string display of alert(s)
           foreach (vwCDRLAlert cdrl in loCDRLAlerts)
            {
                  // here we need to wrap each of these in a hyperlink
                  // @ltContextPath/Application/CDRLEntry?whichaction=update&key="+key
                    if(liCtr > 0)
                        lsbAlertText.Append("|");                    
                    lsbAlertText.Append("*** CDRL Alert For ID : " + cdrl.CDRLID + ". You are " + cdrl.RelationshipTypeDesc + " and have a " + cdrl.KeyDateTypeDesc + " due : " + cdrl.KeyDate + " for Contractor Name : " + cdrl.ContractorNameDesc + ",  Contract # : " + cdrl.ContractNumber) ;
                    liCtr++;
            }
          
            return lsbAlertText.ToString();
        }

        public List<vwCDRLAlert> GetEmailAlerts()      
        {
            Logger.Info("Inside Utilities->GetEmailAlerts");
            DateTime ldCurrentDate = DateTime.Today;
            string ltCurrentDate = ldCurrentDate.ToString("d");    
            var ctxPMDB = new RadarsContractTrackerEntities();
 
            // Get all alerts
           var  loCDRLAlerts = ctxPMDB.Database.SqlQuery<vwCDRLAlert>("SELECT KeyDateID, UserName, EmailReminder, OnScreenReminder, ObligationMet, keydateTypeID, KeyDateTypeDesc, KeyDate, RelationshipTypeID, RelationshipTypeDesc, CDRLID, ContractActionID, CDRLContractLineItemID, CDRLContractLineItemDesc, ContractNumber, ActionTypeID, ActiontypeDesc, ContractTypeID, ContractTypeDesc, ProgramID, ProgramDesc, CurrentProgramPhaseID, CurrentProgramPhaseDesc, ContractorNameID, ContractorNameDesc FROM vwCDRLAlert WHERE EmailReminder = 1 AND ObligationMet = 0 AND KeyDate <= '" + ltCurrentDate + "'").ToList<vwCDRLAlert>();

            int liAlertCount = loCDRLAlerts.Count;

            if(liAlertCount > 0)
            {
                Logger.Info("Inside Utilities->GetEmailAlerts. Returning : " + liAlertCount.ToString() + " Alerts.");
                return loCDRLAlerts;                    
            }
            else
            {
                List<vwCDRLAlert> loCDRLAlerts2 = new List<vwCDRLAlert>();
                return loCDRLAlerts2; 
            }
          
        }

        public personnel GetPersonnelFromAlert(vwCDRLAlert poAlert)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            personnel loPersonnel = ctxPMDB.personnels.SqlQuery("SELECT [PersonnelID], [FirstName], [LastName], [Email], [Phone], [UserName] FROM [dbo].[personnel] WHERE [UserName] = '" + poAlert.UserName + "'").SingleOrDefault();
            if(loPersonnel == null)
            {
                return null;
            }
            else 
                return loPersonnel;
        }

        public bool AddContractAction(int piContractID, string ptActionType, decimal pdContractValue, IPrincipal pipUser)
        {
            RadarsContractTrackerEntities radarsDB = new RadarsContractTrackerEntities();
            bool lbRetVal = false;
            int liGetActionTypeID = GetActionIDByDesc(ptActionType);

            try
            {
                ContractAction loContractAction = new ContractAction { ContractID = piContractID, DateTimeAdded = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), ActionTypeID = GetActionIDByDesc(ptActionType), ContractActionValue = pdContractValue};

                radarsDB.ContractActions.Add(loContractAction);

                radarsDB.SaveChanges();
                if(!AddAuditRecord(pipUser, loContractAction, "Update"))
                {
                        Logger.Error("Error Adding " + loContractAction.GetType().Name + " Audit record for : " + pipUser.Identity.Name);
                        throw new InvalidDataException();
                }

                System.Web.HttpContext.Current.Session["CurrentContractActionID"] = loContractAction.ContractActionID;
                System.Web.HttpContext.Current.Session["DisplayMessage"] = System.Web.HttpContext.Current.Session["DisplayMessage"] + "& " + "Successfully inserted ContractAction record.";
                lbRetVal = true;
            }
            catch(Exception ex)
            {
                lbRetVal = false;
                Logger.Error("Error : Occured in AddContractAction");
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
            }


            return lbRetVal;
        }

        public int GetActionIDByDesc(string ptActionType)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            var loActionTypeRecord = ctxPMDB.ActionTypes.SqlQuery("SELECT [ActionTypeID] ,[ActionTypeDesc] FROM [dbo].[ActionType] WHERE [ActionTypeDesc] = '" + ptActionType + "'").SingleOrDefault();
            if(loActionTypeRecord == null)
                return 0;
            else 
                return loActionTypeRecord.ActionTypeID;
        }

// 20161218 Left off... now just need to add one case per object type

        public bool AddAuditRecord(IPrincipal pipUser, object poModel, string ptAuditType)
        {
            var ctxPMDB = new RadarsContractTrackerEntities();
            bool llRetVal = false;
            string ltSchemaVersion = ConfigurationManager.AppSettings["SchemaVersion"];

            //Determine the type of model passed in and the add audit record for that type...
            string ltObjectType = poModel.GetType().Name;
//            switch (ltObjectType.ToLower())
//            {
//                case "acquisitioncategory":
                    try
                    {
                        AuditTable audittbl = CreateAuditObject(pipUser, poModel, ltObjectType, ltSchemaVersion, ptAuditType);
                        ctxPMDB.AuditTables.Add(audittbl);
                        ctxPMDB.SaveChanges();
                        // Submit the change to the database.
                        llRetVal = true;
                    }
                    catch (DbUpdateException ue)
                    {
                        Logger.Error("DBUpdate Exception : ");
                        Logger.Error(ue);
                        System.Web.HttpContext.Current.Session["DisplayMessage"] = "DBUpdate Exception : " + ue.InnerException.InnerException.Message;
                    }

                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                        throw;
                    }


//                    break;
//                case "ActionType":
//                    llRetVal = true;
//                    break;
//            }

            return llRetVal;
        }

        public AuditTable CreateAuditObject(IPrincipal pipUser, object poModel, string ptObjectType, string ptSchemaVersion, string ptAuditType)
        {
            AuditTable loAuditRecord;

                  StringWriter loStringWriter = new StringWriter();    
                  StringBuilder loHoldCSV = new StringBuilder();
                  StringBuilder loHoldHeaders = new StringBuilder();
                  StringBuilder loHoldAll = new StringBuilder();

                   int liCount = 0;
                   var loPropList = poModel.GetType().GetProperties();
                    int liPropCount = loPropList.Count();
                    string ltHoldValue = "";
                
                   foreach (PropertyInfo propertyInfo in poModel.GetType().GetProperties())
                    {
                        if (propertyInfo.CanRead)
                        {
                            object loValue = propertyInfo.GetValue(poModel, null);
                            if(liCount < liPropCount - 1)
                            {
                                loHoldHeaders.Append("\"" + propertyInfo.Name + "\","); 
                                if(loValue == null)
                                    ltHoldValue = "";
                                else
                                    ltHoldValue = loValue.ToString();
                                loHoldCSV.Append("\"" +ltHoldValue + "\",");
                            }
                            else
                            {
                                loHoldHeaders.Append("\"" + propertyInfo.Name + "\""); 
                                if(loValue == null)
                                    ltHoldValue = "";
                                else
                                    ltHoldValue = loValue.ToString();
                                loHoldCSV.Append("\"" +ltHoldValue + "\"");
                            }
                
                            liCount++;
                        }
                    }

                    loHoldAll.Append(loHoldHeaders.ToString());
                    loHoldAll.Append(Environment.NewLine);
                    loHoldAll.Append(loHoldCSV.ToString());
        
                    AuditTable audittbl = new AuditTable
                    {
                        ChangedTable = ptObjectType,
                        SchemaVersionID = ptSchemaVersion,
                        ChangedDateTime = DateTime.Now.ToString(),
                        ChangedBy = pipUser.Identity.Name,
                        SerializedChange = loHoldAll.ToString(),
                        UpdateType = ptAuditType
                    };

            return audittbl;
        }

        public string SerializeObject(object poModel, XmlSerializer poXMLSerializer)
        {
//                    var subReq = new MyObject();
                    var xml = "";

                     using(var sww = new StringWriter())
                     {
                         using(XmlWriter writer = XmlWriter.Create(sww))
                         {
                             poXMLSerializer.Serialize(writer, poModel);
                             xml = sww.ToString(); // Your XML
                         }
                     }
            return xml;
        }

        static string ConvertStringArrayToJQGridString(List<string> array)
        {
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();

            builder.Append("[");
            for (int x = 0; x < array.Count(); x++)
            {
                if (x == array.Count() - 1)
                    builder.Append("\"" + array[x] + "\"]");
                else
                    builder.Append("\"" + array[x] + "\",");
            }
            return builder.ToString();
        }

          public bool IsContextDirty(DbContext context)
          {
            IEnumerable<DbEntityEntry> res = from e in context.ChangeTracker.Entries()
                where e.State.HasFlag(EntityState.Added) ||
                    e.State.HasFlag(EntityState.Modified) ||
                    e.State.HasFlag(EntityState.Deleted)
                select e;

            if (res.Any())
                return true;

            return false;      
        }
    }


    public static class Logger
    {
        private static log4net.ILog Log { get; set; }

        static Logger()
        {
            Log = log4net.LogManager.GetLogger(typeof(Logger));
        }

        public static void Error(object msg)
        {
            Log.Error("Error: " + msg);
        }

        public static void Error(object msg, Exception ex)
        {
            Log.Error(msg, ex);
        }

        public static void Error(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        public static void Info(object msg)
        {
            Log.Info("Info: " + msg);
        }

        public static void Warn(object msg)
        {
            Log.Info("Warn: " + msg);
        }
    }
}