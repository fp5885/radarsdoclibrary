﻿namespace RadarsDocLibrary.Models
{  
public class JsTreePost
 {
    public string id { get; set; }
    public string text { get; set; }
    public string icon { get; set; }
    public JSLIAttribute li_attr { get; set; }
    public JSAAttribute a_attr { get; set; }
    public JSState state { get; set; }
    public JSData data { get; set; }
    public string parent { get; set; }
    public string type { get; set; }
 }
//{"id":"ajson1","text":"Root","icon":true,"li_attr":{"id":"ajson1"},"a_attr":{"href":"#","id":"ajson1_anchor"},"state":{"loaded":true,"opened":true,"selected":false,"disabled":false},"data":{},"parent":"#","type":"folder"}
public class JSLIAttribute
{
    public string nodeid {get; set;}
}
public class JSAAttribute
{
    public string hrefid {get; set;}
    public string anchorid {get; set;}
}
public class JSState
{
//    "state":{"loaded":true,"opened":true,"selected":false,"disabled":false}
    public string isloaded {get; set;}
    public string isopened {get; set;}
    public string isselected {get; set;}
    public string isdisabled {get; set;}
}
public class JSData
{
    public string docid {get; set;}
    public string parentid {get; set;}
}

}
