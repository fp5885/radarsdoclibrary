﻿namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContractActionListModel
    {
        public int ContractID { get; set; }
        public string ContractNumber { get; set; }
        public string ContractDT { get; set; }
        public int ContractActionID { get; set; }
        public string ContractActionDT { get; set; }
        public string ActionTypeDesc { get; set; }
        public string DOTONumber { get; set; }
        public string ModNumber { get; set; }
        public string ActionDescription { get; set; }
        public Nullable<System.DateTime> AwardDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string ContractActionNotes { get; set; }
        public  Nullable<decimal>  ContractActionValue { get; set; }
        public string ContractTypeDesc { get; set; }
        public string AcquisitionCategoryDesc { get; set; }
        public string ProgramDesc { get; set; }
        public string CurrentProgramPhaseDesc { get; set; }
        public string ContractorNameDesc { get; set; }
        public string ContractDescription { get; set; }
        public string ContractNotes { get; set; }
        public Nullable<decimal> ContractValue { get; set; }
    }
}
