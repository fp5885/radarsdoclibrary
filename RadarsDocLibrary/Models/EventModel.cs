﻿namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;

    public partial class EventModel
    {
        public string changedate { get; set; }
        public string serialnumber { get; set; }
        public string gainingmacom {get; set;}
        public string gainingunitname {get; set;}
        public string gainingunitpoc {get; set;}
        public string uic {get; set;}
        public string dodaac {get; set;}
        public string fieldingcoordinator {get; set;}
        public string fieldertraining {get; set;}
        public string missionstartdate {get; set;}
        public string missionenddate {get; set;}
        public string pdmevent { get; set; }
        public string location { get; set; }
        public string hardwareconfig { get; set; }
        public string softwareconfig { get; set; }
        public string notes { get; set; }
        public string lastupdatedby { get; set; }
        public string  dd250date { get; set; }
        public string livefiretestdate { get; set; }
        public string fieldingtrainer2 { get; set; }
        public string nsmmaintpartnum1 { get; set; }
        public string nsnmaintpartnum2 { get; set; }
        public string nsnmaintpartnum3 { get; set; }
        public Nullable<bool> isdeleted { get; set; }
    }
}
