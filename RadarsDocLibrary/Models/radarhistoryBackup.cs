﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;

namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;

    public partial class radarfleethistorybackup
    {
        [Display(Name = "Change Date")]
        public string changedate { get; set; }
        [StringLength(25)]
        [Display(Name = "System Name")]
        public string systemname { get; set; }
        [StringLength(25)]
        [Display(Name = "LIN")]
        public string lin { get; set; }
        [StringLength(50)]
        [Display(Name = "NSN")]
        public string nsn { get; set; }
        [StringLength(50)]
        [Display(Name = "Serial Number")]
        public string serialnumber { get; set; }
        [StringLength(50)]
        [Display(Name = "Primary Mover")]
        public string primarymoverid { get; set; }
        [StringLength(50)]
        [Display(Name = "Power Unit Serial")]
        public string powerunitserial { get; set; }
        [StringLength(50)]
        [Display(Name = "Support Mover Serial")]
        public string supportmoverserial { get; set; }
        [StringLength(50)]
        [Display(Name = "Shelter Serial")]
        public string shelterserialnumber { get; set; }
        [StringLength(50)]
        [Display(Name = "Gaining Macom")]
        public string gainingmacom { get; set; }
        [StringLength(50)]
        [Display(Name = "Gaining Unit")]
        public string gainingunitname { get; set; }
        [StringLength(50)]
        [Display(Name = "Location")]
        public string location { get; set; }
        [StringLength(50)]
        [Display(Name = "Gaining Unit POC")]
        public string gainingunitpoc { get; set; }
        [StringLength(50)]
        [Display(Name = "UIC")]
        public string uic { get; set; }
        [StringLength(50)]
        [Display(Name = "DODAAC")]
        public string dodaac { get; set; }
        [StringLength(10)]
        [Display(Name = "FieldTrainer1")]
        public string fieldertraining { get; set; }
        [Display(Name = "MissionStart Date")]
        public Nullable<System.DateTime> missionstartdate { get; set; }
        [Display(Name = "MissionEnd Date")]
        public Nullable<System.DateTime> missionenddate { get; set; }
        [StringLength(10)]
        [Display(Name = "Hardware Config")]
        public string hardwareconfig { get; set; }
        [StringLength(10)]
        [Display(Name = "Software Config")]
        public string softwareconfig { get; set; }
        [Display(Name = "Notes")]
        public string notes { get; set; }
        [StringLength(10)]
        [Display(Name = "Fielding Coordinator")]
        public string fieldingcoordinator { get; set; }
        [StringLength(25)]
        [Display(Name = "PDM Event")]
        public string pdmevent { get; set; }
        public string lastupdatedby { get; set; }
        [Display(Name = "DD250 Date")]
        public Nullable<System.DateTime> dd250date { get; set; }
        [Display(Name = "LiveFireTest Date")]
        public Nullable<System.DateTime> livefiretestdate { get; set; }
        [StringLength(10)]
        [Display(Name = "Fielding Trainer 2")]
        public string fieldingtrainer2 { get; set; }
    }
}
