﻿namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;

    public partial class CDRLListModel
    {
        public int CDRLID { get; set; }
        public int ContractActionID { get; set; }
        public string CDRLDT { get; set; }
        public int CDRLContractLineItemID { get; set; }
        public string CDRLContractLineItemDesc { get; set; }
        public string DataItemNo { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Authority { get; set; }
        public string VendorContractNumber { get; set; }
        public string ContractRefNo { get; set; }
        public Nullable<int> CDRLRequiringOfficeID { get; set; }
        public string CDRLRequiringOfficeDesc { get; set; }
        public Nullable<int> CDRLDD250ID { get; set; }
        public string CDRLDD250Desc { get; set; }
        public Nullable<int> CDRLDistStatementID { get; set; }
        public string CDRLDistStatementDesc { get; set; }
        public Nullable<int> CDRLFrequencyID { get; set; }
        public string CDRLFrequencyDesc { get; set; }
        public Nullable<System.DateTime> AsOfDate { get; set; }
        public Nullable<int> CDRLStatusID { get; set; }
        public string CDRLStatusDesc { get; set; }
        public string DetailsAndRemarks { get; set; }
        public string RoleID { get; set; }
        public Nullable<int> CDRLAppCodeid { get; set; }
        public string CDRLAppCodeDesc { get; set; }
        public DateTime POPStart { get; set; }
        public DateTime POPEnd { get; set; }
    }
    
}
