﻿
namespace RadarsDocLibrary.Models
{   
    using System;
    using System.Collections.Generic;

    public partial class DocumentUploadGridList
    {
        public int DocumentID { get; set; }
        public string DateTimeAdded { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
    }
}
