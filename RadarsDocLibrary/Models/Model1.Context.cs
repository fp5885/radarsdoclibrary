﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RadarsDocLibrary.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class RadarsFleetManagerEntities : DbContext
    {
        public RadarsFleetManagerEntities()
            : base("name=RadarsFleetManagerEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<documentupload> documentuploads { get; set; }
        public virtual DbSet<DocumentUploads2> DocumentUploads2 { get; set; }
        public virtual DbSet<ecutype> ecutypes { get; set; }
        public virtual DbSet<fieldingcoordinator> fieldingcoordinators { get; set; }
        public virtual DbSet<fieldtrainer> fieldtrainers { get; set; }
        public virtual DbSet<generatortype> generatortypes { get; set; }
        public virtual DbSet<hardwareconfig> hardwareconfigs { get; set; }
        public virtual DbSet<lindesc> lindescs { get; set; }
        public virtual DbSet<nsndesc> nsndescs { get; set; }
        public virtual DbSet<pdmevent> pdmevents { get; set; }
        public virtual DbSet<pmradarstracker> pmradarstrackers { get; set; }
        public virtual DbSet<radarfleethistory> radarfleethistories { get; set; }
        public virtual DbSet<sitecontent> sitecontents { get; set; }
        public virtual DbSet<softwareconfig> softwareconfigs { get; set; }
        public virtual DbSet<systemnamedesc> systemnamedescs { get; set; }
        public virtual DbSet<toolkittype> toolkittypes { get; set; }
        public virtual DbSet<trailertype> trailertypes { get; set; }
        public virtual DbSet<vehicletype> vehicletypes { get; set; }
        public virtual DbSet<eventdocument> eventdocuments { get; set; }
        public virtual DbSet<radareventjoin> radareventjoins { get; set; }
        public virtual DbSet<DocumentCat> DocumentCats { get; set; }
    }
}
