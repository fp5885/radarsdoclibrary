﻿namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContractListModel
    {
        public int ContractID { get; set; }
        public string ContractNumber { get; set; }
        public string DateTimeAdded { get; set; }
        public string ContractTypeDesc { get; set; }
        public string AcquisitionCategoryDesc { get; set; }
        public string ProgramDesc { get; set; }
        public string CurrentProgramPhaseDesc { get; set; }
        public string ContractorNameDesc { get; set; }
        public string ContractDescription { get; set; }
        public string ContractNotes { get; set; }
        public Nullable<decimal> ContractValue { get; set; }
        public Nullable<System.DateTime> POPStart { get; set; }
        public Nullable<System.DateTime> POPEnd { get; set; }
        public string SolicitationNo { get; set; }
        public string RoleID {get; set; }
    }
}
