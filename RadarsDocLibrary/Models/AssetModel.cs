﻿using System.ComponentModel.DataAnnotations;

namespace RadarsDocLibrary.Models
{   
    using System;
    using System.Collections.Generic;

    public partial class AssetModel
    {
        [Required]
        [StringLength(25)]
        [Display(Name = "SystemName")]
        public string systemname { get; set; }
        [Required]
        [StringLength(25)]
        [Display(Name = "LIN")]
        public string lin { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "NSN")]
        public string nsn { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "SerialNumber")]
        public string serialnumber { get; set; }
        [StringLength(50)]
        [Display(Name = "Vehicle1ID")]
        public string primarymoverid { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator1ID")]
        public string powerunitserial { get; set; }
        [StringLength(50)]
        [Display(Name = "Vehicle2ID")]
        public string supportmoverserial { get; set; }
        [StringLength(50)]
        [Display(Name = "ShelterSerial")]
        public string shelterserialnumber { get; set; }
        [StringLength(50)]
        [Display(Name = "GainingMacom")]
        public string gainingmacom { get; set; }
        [StringLength(50)]
        [Display(Name = "GainingUnit")]
        public string gainingunitname { get; set; }
        [StringLength(50)]
        [Display(Name = "Location")]
        public string location { get; set; }
        [StringLength(50)]
        [Display(Name = "GainingUnitPOC")]
        public string gainingunitpoc { get; set; }
        [StringLength(50)]
        [Display(Name = "UIC")]
        public string uic { get; set; }
        [StringLength(50)]
        [Display(Name = "DODAAC")]
        public string dodaac { get; set; }
        [StringLength(10)]
        [Display(Name = "FielderTraining")]
        public string fieldertraining { get; set; }
        public Nullable<System.DateTime> missionstartdate { get; set; }
        public Nullable<System.DateTime> missionenddate { get; set; }
        [Required]
        [StringLength(10)]
        [Display(Name = "HardwareConfig")]
        public string hardwareconfig { get; set; }
        [Required]
        [StringLength(10)]
        [Display(Name = "SoftwareConfig")]
        public string softwareconfig { get; set; }
        [Display(Name = "Notes")]
        public string notes { get; set; }
        [StringLength(10)]
        [Display(Name = "FieldingCoord")]
        public string fieldingcoordinator { get; set; }
        [StringLength(25)]
        [Display(Name = "PDMEvent")]
        public string pdmevent { get; set; }
        [Display(Name = "CabKit")]
        public Nullable<bool> cabkit { get; set; }
        [StringLength(50)]
        [Display(Name = "PDUSerial")]
        public string pduserial { get; set; }
        [StringLength(50)]
        [Display(Name = "VehicleMountSerial")]
        public string vehiclemountserial { get; set; }
        
        public Nullable<bool> isdeleted { get; set; }

        [StringLength(50)]
        [Display(Name = "Vehicle1Type")]
        public string vehicle1type { get; set; }
        [StringLength(50)]
        [Display(Name = "Vehicle2Type")]
        public string vehicle2type { get; set; }
        [StringLength(50)]
        [Display(Name = "Vehicle3ID")]
        public string vehicle3id { get; set; }
        [StringLength(50)]
        [Display(Name = "Vehicle3Type")]
        public string vehicle3type { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator1Type")]
        public string generator1type { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator2ID")]
        public string generator2id { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator2Type")]
        public string generator2type { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator3ID")]
        public string generator3id { get; set; }
        [StringLength(50)]
        [Display(Name = "Generator3Type")]
        public string generator3type { get; set; }
        [StringLength(50)]
        [Display(Name = "ECU1ID")]
        public string ecu1id { get; set; }
        [StringLength(50)]
        [Display(Name = "ECU1Type")]
        public string ecu1type { get; set; }
        [StringLength(50)]
        [Display(Name = "ECU2ID")]
        public string ecu2id { get; set; }
        [StringLength(50)]
        [Display(Name = "ECU2Type")]
        public string ecu2type { get; set; }
        [StringLength(50)]
        [Display(Name = "Trailer1ID")]
        public string trailer1id { get; set; }
        [StringLength(50)]
        [Display(Name = "Trailer1Type")]
        public string trailer1type { get; set; }
        [StringLength(50)]
        [Display(Name = "Trailer2ID")]
        public string trailer2id { get; set; }
        [StringLength(50)]
        [Display(Name = "Trailer2Type")]
        public string trailer2type { get; set; }
        [StringLength(50)]
        [Display(Name = "ToolkitID")]
        public string toolkitid { get; set; }
        [StringLength(50)]
        [Display(Name = "ToolkitType")]
        public string toolkittype { get; set; }
    }
}
