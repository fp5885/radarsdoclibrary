﻿namespace RadarsDocLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContractActionJSON
    {

        public string __RequestVerificationToken {get; set; }
        public string valueshavechanged {get; set;}
        public int ContractActionID { get; set; }
        public int ContractID { get; set; }
        public string DateTimeAdded { get; set; }
        public int ActionTypeID { get; set; }
        public string DOTONumber { get; set; }
        public string ModNumber { get; set; }
        public string ActionDescription { get; set; }
        public Nullable<System.DateTime> AwardDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string ContractActionNotes { get; set; }
        public Nullable<decimal> ContractActionValue { get; set; }
        public string RoleID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
