﻿using System.ComponentModel.DataAnnotations;

namespace RadarsDocLibrary.Models
{   
    using System;
    using System.Collections.Generic;

    public partial class RoleModel
    {
//        [Required]
//        [StringLength(128)]
//        [Display(Name = "Role ID")]
//        public string RoleID { get; set; }
        [Required]
        [StringLength(256)]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }
}
