﻿using Quartz;
using Quartz.Impl;
using System;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace RadarsDocLibrary
{
    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<EmailJob>().Build();
            int liEmailHour = Int32.Parse(ConfigurationManager.AppSettings["EmailHour"]);
            int liEmailMinute = Int32.Parse(ConfigurationManager.AppSettings["EmailMinute"]);
            
            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(liEmailHour, liEmailMinute))
                  )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }

}