USE [RadarsFleetManager]
GO

CREATE TABLE [dbo].[DocumentCat](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryDesc] [varchar](100) NOT NULL,
	[ParentCatID] [int] NOT NULL,
 CONSTRAINT [pidx_DocumentCat_CategoryID] PRIMARY KEY CLUSTERED 
 (
	[CategoryID] ASC
 )
)
GO
